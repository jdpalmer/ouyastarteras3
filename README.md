# OVERVIEW

This project is a full featured "starter kit" for Ouya development using AS3.

OuyaStarter4AS3 provides:

1. A Makefile for building projects
2. Basic Starling setup with stage resize support
3. Basic Ouya Gamepad support via the Gamepad library.
4. Remote debugging support via the MonsterDebugger library.
5. Lots of comments and references.

OuyaStarter4AS3 includes precompiled SWCs for:

* Away3D 4.1.4
* DragonBones 2.3.1
* Feathers 1.1.0
* MonsterDebugger 3.02
* Starling 1.3
* HiResStats 201205
* Gamepad 130923

![Demo](http://bitmasq.com/images/OuyaStarter4AS3.png)

# INSTRUCTIONS

1. Install the Flex SDK.  The Makefile defaults to C:\flex-sdk on
Windows and /opt/flex-sdk on everything else.  If you install it
somewhere else, just update the FLEX_SDK variable in the Makefile.
The installer is available here:

    http://flex.apache.org/

2. (OPTIONAL) Install the Android SDK. You only need this if you want
to install your application via USB for quick debugging.  The Makefile
defaults to c:\android-sdk on Windows and /opt/android-sdk on
everything else.  You can change this with the ANDROID_SDK variable in
the Makefile.

3. You need GNU Make.  This is part of XCode on OS X if you install
the command line utilities.  And it's typically available by default
on Linux.  On Windows it is part of cygwin and msys but if you don't
have those installed, you can download a precompiled exe from me.  
Simply copy this file in your path (e.g., C:\WINDOWS\sytem32).

    http://bitmasq.com/files/make.exe

3. To build the application SWF.  Open a Command Prompt (windows) or a
terminal (*nix) and type:

    make

4. To test the application SWF locally type:

    make test

5. To build an APK for deployment type:

    make apk

# AUTHORS

* Glue code by James Dean Palmer
* Gamepad class inspired in part by Zeh Fernando
* ControllerSprite graphics by Mike McMullin
* Included SWCs by their respective authors

# LICENSING

Since OuyaStarter4AS3 is intended to be educational in nature and used as a starter for other projects the source files in the src directory are placed in the public domain per this waiver:

To the extent possible under law, the authors have waived all copyright and related or neighboring rights to the AS3 code provided in the OuyaStarter4AS3 'src' directory.

Compiled code in the 'vendor' directory and images in the 'images' directory is copyrighted and/or licensed by their respective authors.
