##
## This section has common project settings you may need to change.
##

ifeq ($(OS),Windows_NT)
   FLEX_SDK=c:/FlexSDK
   ANDROID_SDK=c:/android-sdk
else
   FLEX_SDK=/opt/flex-sdk
   ANDROID_SDK=/opt/android-sdk
endif

APP=OuyaStarter4AS3

APP_XML=$(APP).xml

SOURCES=src/Startup.as src/Root.as src/ControllerSprite.as

##
## It's less common that you would need to change anything after this line.
##

SIGN_CERT=sign.pfx

SIGN_PWD=abc123

SWF_VERSION=11.8

ADL=$(FLEX_SDK)/bin/adl

ADT=$(FLEX_SDK)/bin/adt

AMXMLC=$(FLEX_SDK)/bin/amxmlc

##
## Build rules
##

all: $(APP).swf

apk: $(APP).apk

clean:
	rm -rf $(APP).swf $(APP).apk

test: $(APP).swf
	$(ADL) -profile tv -screensize 960x540:960x540 $(APP_XML)

testHi: $(APP).swf
	$(ADL) -profile tv -screensize 1920x1080:1920x1080 $(APP_XML)

sign.pfx:
	$(ADT) -certificate -validityPeriod 25 -cn SelfSigned 1024-RSA $(SIGN_CERT) $(SIGN_PWD)

install: $(APP).apk
	$(ADT) -installApp -platform android -platformsdk $(ANDROID_SDK) -package $(APP).apk

$(APP).swf: $(SOURCES)
	$(AMXMLC) \
	-library-path+="vendor/Away3D_4_1_4.swc" \
	-library-path+="vendor/DragonBones_2_3_1.swc" \
	-library-path+="vendor/Feathers_1_1_0.swc" \
	-library-path+="vendor/MonsterDebugger_3_2.swc" \
	-library-path+="vendor/Starling_1_3.swc" \
	-library-path+="vendor/Stats_201205.swc" \
	-library-path+="vendor/Gamepad_130923.swc" \
	src/Startup.as -output $(APP).swf

$(APP).apk: $(APP).swf sign.pfx
	$(ADT) -package -target apk-captive-runtime -storetype pkcs12 -keystore $(SIGN_CERT) $(APP).apk $(APP_XML) $(APP).swf images

# Code to build Stats pack from:
# https://github.com/MindScriptAct/Advanced-hi-res-stats
# /opt/flex-sdk/bin/acompc -source-path src -include-classes utils.debug.Stats -output Stats_201205.swc
