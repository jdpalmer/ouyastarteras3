package {

  import bitmasq.Gamepad;

  import com.demonsters.debugger.MonsterDebugger;

  import flash.display.Sprite;
  import flash.display.StageAlign;
  import flash.display.StageScaleMode;
  import flash.display.Bitmap;
  import flash.events.Event;
  import flash.filesystem.File;
  import flash.geom.Rectangle;
  import flash.system.Capabilities;
  import flash.ui.Keyboard;
  import flash.utils.ByteArray;
  import flash.utils.Dictionary;

  import starling.core.Starling;
  import starling.events.Event;
  import starling.textures.Texture;
  import starling.utils.AssetManager;

  import utils.debug.Stats;


  // SWF metadata parameters are described here:
  // http://goo.gl/hYqRA
  [SWF(frameRate="60", backgroundColor="#ffffff", width=1920, height=1080)]
  public class Startup extends Sprite {

    private const debugEnabled:Boolean = true;
    private const debugLocal:Boolean = true;
    private const debugIP:String = "192.168.0.2";

    [Embed(source="../images/bitmasq_logo_screen.png")]
    private static var Background:Class;

    public function Startup() {
      super();

      stage.align = StageAlign.TOP_LEFT;
      stage.scaleMode = StageScaleMode.NO_SCALE;
      stage.addEventListener(flash.events.Event.RESIZE, function(e:flash.events.Event): void {
        Starling.current.viewPort = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight);
        background.width = stage.stageWidth;
        background.height = stage.stageHeight;
      });

      var background:Bitmap = new Background();
      
      background.x = 0;
      background.y = 0;
      background.width  = stage.stageWidth;
      background.height = stage.stageHeight;
      background.smoothing = true;
      addChild(background);

      Gamepad.traceFunction = function(str:String):void {
        MonsterDebugger.trace(this, str);
      };

      Gamepad.inspectFunction = function(o:Object):void {
        MonsterDebugger.inspect(o);
      };     

      Root.gamepad = Gamepad.init(stage);

      /*
      var map:Dictionary = new Dictionary();
      map[Keyboard.LEFT] = Gamepad.D_LEFT;
      map[Keyboard.RIGHT] = Gamepad.D_RIGHT;
      map[Keyboard.UP] = Gamepad.D_UP;
      map[Keyboard.DOWN] = Gamepad.D_DOWN;
      Root.gamepad.addKeyController(map);
      */

      var _starling:Starling = new Starling(Root, stage);
      _starling.antiAliasing = 1;

      var appDir:File = File.applicationDirectory;
      Root.assets = new AssetManager(1.0);
      Root.assets.verbose = Capabilities.isDebugger;
      Root.assets.enqueue(
        appDir.resolvePath("images")
      );

      _starling.addEventListener(
        starling.events.Event.ROOT_CREATED, 
        function onRootCreated(event:Object, app:Root):void {
          _starling.removeEventListener(starling.events.Event.ROOT_CREATED, onRootCreated);
          app.start(background);
          _starling.start();

          MonsterDebugger.enabled = debugEnabled;
          if (debugLocal) {
            MonsterDebugger.initialize(app);
          } else {
            MonsterDebugger.initialize(app, debugIP);
          }

        });

      var stats:Stats = new Stats();
      stage.addChild(stats);

    }
    
  }
}
