package {

  import bitmasq.Gamepad;

  import com.demonsters.debugger.MonsterDebugger;

  import flash.utils.Dictionary;

  import starling.display.Image;
  import starling.display.Sprite;
  import starling.events.Event;
  import starling.textures.Texture;

  /**
   * Graphic representation of Ouya Controller
   * @author Mike McMullin
   */
  public class ControllerSprite extends Sprite {

    public var joysticks:Array;
    public var buttons:Dictionary = new Dictionary();
    
    public function ControllerSprite() {
      super();
      create();
    }
    
    public function create():void {
      var i:Number;
      
      addChild(new Image(Root.assets.getTexture("cutter")));
      
      joysticks = new Array(
        createStick("l_stick"),
        createStick("r_stick")
      );

      buttons[Gamepad.A_DOWN] = createButton("o");
      buttons[Gamepad.A_LEFT] = createButton("u");
      buttons[Gamepad.A_UP] = createButton("y");
      buttons[Gamepad.A_RIGHT] = createButton("a");

      buttons[Gamepad.LB] = createButton("lb");
      buttons[Gamepad.RB] = createButton("rb");
      buttons[Gamepad.LT] = createButton("lt");
      buttons[Gamepad.RT] = createButton("rt");

      buttons[Gamepad.D_UP] = createButton("dpad_up");
      buttons[Gamepad.D_DOWN] = createButton("dpad_down");
      buttons[Gamepad.D_LEFT] = createButton("dpad_left");
      buttons[Gamepad.D_RIGHT] = createButton("dpad_right");

      // TODO:
      // buttons[Gamepad.LSTICK] = createButton("lstick");
      // buttons[Gamepad.RSTICK] = createButton("rstick");
      // buttons[Gamepad.MENU] = createButton("menu");
            
    }

    private function createStick(asset:String):Image {

      var img:Image = new Image(Root.assets.getTexture(asset));
      addChild(img);
      return img;

    }

    private function createButton(asset:String):Image {

      var img:Image = new Image(Root.assets.getTexture(asset));
      img.visible = false;
      addChild(img);
      return img;

    }
    
    public function updateButton(buttonID:Number, value:Number):void {

      if (buttons[buttonID]) {
        buttons[buttonID].visible = (value != 0);
        return;
      }

      value *= 10.0;

      switch (buttonID) {

      case Gamepad.LSTICK_X:
        joysticks[0].x = value;
        return;

      case Gamepad.LSTICK_Y:
        joysticks[0].y = value;
        return;

      case Gamepad.RSTICK_X:
        joysticks[1].x = value;
        return;

      case Gamepad.RSTICK_Y:
        joysticks[1].y = value;
        return;

      }

      MonsterDebugger.trace(this, 'Uknown button id: $buttonID');

      return;

    }        
  }
}
