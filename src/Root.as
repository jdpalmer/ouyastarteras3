package {

  import bitmasq.Gamepad;
  import bitmasq.GamepadEvent;

  import com.demonsters.debugger.MonsterDebugger;

  import flash.display.Bitmap;
  import flash.events.Event;
  import flash.geom.Rectangle;
  import flash.utils.ByteArray;

  import starling.core.Starling;
  import starling.display.Sprite;
  import starling.utils.AssetManager;
  import starling.animation.Juggler;
  import starling.animation.Transitions;
  import starling.animation.Tween;
  import starling.animation.DelayedCall;

  public class Root extends Sprite {

    public static var gamepad:Gamepad;
    public static var assets:AssetManager;
    public static var controllers:Array = [];

    public function Root() {

      super();
      gamepad.addEventListener(GamepadEvent.CHANGE, onChange);

    }

    public function start(background:Bitmap):void {

      assets.loadQueue(function onProgress(ratio:Number):void {
        if (ratio == 1) {

          for (var i:Number = 0; i < 4; i++) {
            controllers.push(createController(i));
            addChild(controllers[i]);
          }

          Starling.juggler.tween(
            background,
            1.0, {
              transition: Transitions.EASE_IN_OUT,
              delay: 0.15,
              alpha: 0
            });
        }
      });

    }

    private function createController(id:Number):ControllerSprite {

      var controller:ControllerSprite = new ControllerSprite();
      controller.y = 300;
      controller.x = id  * 400 + 160;
      return controller;

    }

    private function onChange(event:GamepadEvent):void {

      if (event.deviceIndex < Root.controllers.length) {
        Root.controllers[event.deviceIndex].updateButton(event.control, event.value);
      }

    }

  }
}
